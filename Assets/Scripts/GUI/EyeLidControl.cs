﻿using UnityEngine;
using System.Collections;

public class EyeLidControl : MonoBehaviour {

    private float initialY;
	// Use this for initialization
	void Start () 
    {
        float yScale = 1f;
        initialY = transform.localPosition.y;
        if(transform.localPosition.y == 1f)
        {
            yScale = -1;
        }
        transform.GetComponent<GUITexture>().pixelInset = new Rect(0, 0, Screen.width, Screen.height * yScale);
        transform.localPosition = new Vector3(0f, 0.5f, 0f);
        GameManager.Instance.BeganSquinting += new GameManager.SquintEvent(CloseEyeLid);
        GameManager.Instance.StoppedSquinting += new GameManager.SquintEvent(OpenEyeLid);
        GameManager.Instance.PlayerDied += new GameManager.DeathEvent(CloseEyeLid);
        OpenEyeLid();
	}

    public void CloseEyeLid()
    {
        LeanTween.cancel(gameObject);
        LeanTween.moveLocalY(gameObject, initialY, 0.5f).setEase(LeanTweenType.easeOutQuad);
    }

    public void OpenEyeLid()
    {
        LeanTween.cancel(gameObject);
        LeanTween.moveLocalY(gameObject, 0.5f, 0.5f).setEase(LeanTweenType.easeOutQuad);
    }
	
}
