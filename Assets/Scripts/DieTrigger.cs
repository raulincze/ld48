﻿using UnityEngine;
using System.Collections;

public class DieTrigger : MonoBehaviour {

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            GameManager.Instance.IsDead = true;
        }
    }
}
