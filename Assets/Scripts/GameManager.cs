﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

    public event SquintEvent BeganSquinting;
    public event SquintEvent StoppedSquinting;
    public event EyeStrainChangedEvent ModifiedEyeStrain;
    public event DeathEvent PlayerDied;

    public delegate void SquintEvent();
    public delegate void EyeStrainChangedEvent(float value);
    public delegate void DeathEvent();

    public float eyeStrainRegenRate = 2.0f;
    public float eyeStrength = 8f;
    public float eyeStrain;
    private bool waitToRecover;
    private bool isDead;

    public static GameManager Instance
    {
        get { return instance; }
    }
    private bool isSquinting;
    private static GameManager instance;

    public bool IsSquinting
    {
        get { return isSquinting; }
        set 
        {
            bool val = value; ;
            if (val == true)
            {
                if (!waitToRecover)
                {
                    BeganSquinting.Invoke();
                }
                else
                {
                    val = false;
                }
            }
            else
            {
                StoppedSquinting.Invoke();
            }
            isSquinting = val;
        }
    }

    public bool IsDead
    {
        get { return isDead; }
        set
        {
            if (value == true)
            {
                PlayerDied.Invoke();
            }
            isDead = value;
        }
    }

    public void Awake()
    {
        Time.timeScale = 1;
        instance = this;
        isSquinting = false;
        isDead = false;
        waitToRecover = false;
        eyeStrain = eyeStrength;
    }

    public void Update()
    {
        if (isSquinting)
        {
            if (eyeStrain <= 0)
            {
                eyeStrain = 0;
                IsSquinting = false;
                waitToRecover = true;
            }
            else
            {
                eyeStrain -= Time.deltaTime;
                ModifiedEyeStrain.Invoke(eyeStrain);
            }
        }
        else
        {
            if (eyeStrain < eyeStrength)
            {
                eyeStrain += Time.deltaTime * eyeStrainRegenRate;
                ModifiedEyeStrain.Invoke(eyeStrain);
            }
            else
            {
                eyeStrain = eyeStrength;
                waitToRecover = false;
            }
        }
#if !UNITY_WEBPLAYER
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            IsDead = true;
        } 
#endif
    }
}
