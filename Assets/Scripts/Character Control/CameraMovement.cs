﻿using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour
{
    public float sensitivityX = 15F;
    public float sensitivityY = 15F;

    public float minimumX = -360F;
    public float maximumX = 360F;

    public float minimumY = -60F;
    public float maximumY = 60F;

    float rotationY = 0F;

    void Awake()
    {
        Screen.lockCursor = true;
    }

    void Update()
    {

        float rotationX = transform.parent.localEulerAngles.y + Input.GetAxis("Mouse X") * sensitivityX;

        rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
        rotationY = Mathf.Clamp(rotationY, minimumY, maximumY);

        transform.localEulerAngles = new Vector3(-rotationY, 0, 0);
        transform.parent.localEulerAngles = new Vector3(0, rotationX, 0);
    }
}
