﻿

Shader "Custom/River" {
Properties {
	_MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
	_Detail ("Detail Texture", 2D) = "white" {}
	_Speed ("Alpha cutoff", Range(0,10)) = 0.5
}
SubShader {
	Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
	Blend SrcAlpha OneMinusSrcAlpha

	Lighting Off
	Cull Off

	Pass {  
		CGPROGRAM
// Upgrade NOTE: excluded shader from DX11 and Xbox360; has structs without semantics (struct v2f members position)
#pragma exclude_renderers d3d11 xbox360
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata_t {
				float4 vertex : POSITION;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f {
				float4 vertex : SV_POSITION;
				half2 texcoord : TEXCOORD0;
				float4 position;
			};

			sampler2D _MainTex;
			sampler2D _Detail;
			float4 _MainTex_ST;
			fixed _Speed;

			v2f vert (appdata_t v)
			{
				v2f o;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.texcoord = TRANSFORM_TEX(v.texcoord, _MainTex);
				o.position = o.vertex;
				return o;
			}
			
			fixed4 frag (v2f i) : COLOR
			{
				float4 clipSpace = i.position;
				float2 offset = float2(-_Time.y * _Speed, 0);
    			clipSpace.xy /= clipSpace.w;
    			clipSpace.xy = _ScreenParams.xy * 0.001 * (clipSpace.xy);
				fixed4 col = tex2D(_MainTex, i.texcoord + offset);
				float4 d = tex2D (_Detail, clipSpace.xy); 
				d = d*0.5 + col*0.5;
				d.a = col.a;
				return d ;
			}
		ENDCG
	}
}
}