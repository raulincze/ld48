﻿using UnityEngine;
using System.Collections;

public class SquintController : MonoBehaviour {

    public float squintFieldOfView = 40f;
    private float initialFieldOfView;

    void Awake()
    {
        initialFieldOfView = Camera.main.fieldOfView;
        GameManager.Instance.BeganSquinting += new GameManager.SquintEvent(NarrowFieldOfView);
        GameManager.Instance.StoppedSquinting += new GameManager.SquintEvent(ResetFieldOfView);
    }

    // Update is called once per frame
	void Update () {
        if (Input.GetButton("Squint"))
        {
            if (!GameManager.Instance.IsDead)
            {
                Screen.lockCursor = true;
            }
            if (!GameManager.Instance.IsSquinting)
            {
                GameManager.Instance.IsSquinting = true;
            }
        }
        else 
        {
            if (GameManager.Instance.IsSquinting)
            {
                GameManager.Instance.IsSquinting = false;
            }
        }
	}

    public void NarrowFieldOfView()
    {
        LeanTween.cancel(gameObject);
        LeanTween.value(gameObject, "SetFieldOfView", Camera.main.fieldOfView, squintFieldOfView, 0.5f);
    }

    public void ResetFieldOfView()
    {
        LeanTween.cancel(gameObject);
        LeanTween.value(gameObject, "SetFieldOfView", Camera.main.fieldOfView, initialFieldOfView, 0.5f);
    }

    private void SetFieldOfView(float value)
    {
        Camera.main.fieldOfView = value;
    }
}
