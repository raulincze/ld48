﻿using UnityEngine;
using System.Collections;

public class EyeClosingControl : MonoBehaviour {

    private GUITexture toModify;
    public Transform endGameScreen;
	// Use this for initialization
	void Start () 
    {
        toModify = transform.GetComponent<GUITexture>();
        toModify.pixelInset = new Rect(0, 0, Screen.width, Screen.height);
        GameManager.Instance.PlayerDied += new GameManager.DeathEvent(Die);
	}

    public void Die()
    {
        StartCoroutine(Dying());
    }

    IEnumerator Dying()
    {
        LeanTween.cancel(gameObject);
        LeanTween.value(gameObject, "SetAlpha", toModify.color.a, 1f, 5f);
        yield return new WaitForSeconds(4f);
        endGameScreen.gameObject.SetActive(true);
        Screen.lockCursor = false;
        Time.timeScale = 0f;
    }

    void SetAlpha(float value)
    {
        Color newColor = toModify.color;
        newColor.a = value;
        toModify.color = newColor;
    }
}
