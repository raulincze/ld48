﻿using UnityEngine;
using System.Collections;

public class FootStepsPlayer : MonoBehaviour {
    public AudioClip[] steps;
    public float timeBetweenSteps;
    public AudioSource source;
    private int lastRnd;
    private float volume;
    private float pitch;
	// Use this for initialization
	void Start () {
        source = GetComponent<AudioSource>();
        volume = source.volume;
        pitch = source.pitch;
        lastRnd = 0;
        StartCoroutine(PlaySteps());
	}

    IEnumerator PlaySteps()
    {
        while (true)
        {
            int rnd;
            do
            {
                rnd = Random.Range(0, steps.Length);
            } while (rnd == lastRnd);
            source.clip = steps[rnd];
            if (PlayerMovement.IsWalking)
            {
                source.Play();
                lastRnd = rnd;
            }
            yield return new WaitForSeconds(timeBetweenSteps + steps[rnd].length);
        }
    }

    void Update()
    {
        Debug.Log(PlayerMovement.IsWalking);
        if (PlayerMovement.IsWalking)
        {
            //source.volume = volume;
        }
        else
        {
           // source.volume = 0;
        }
        if (PlayerMovement.IsRunning)
        {
            source.pitch = pitch + 0.2f;
        }
        else 
        {
            source.pitch = pitch;
        }
    }
}
