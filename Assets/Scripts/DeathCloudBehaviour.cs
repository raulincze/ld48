﻿using UnityEngine;
using System.Collections;

public class DeathCloudBehaviour : MonoBehaviour {

    public int emissionAmount = 3;
    private ParticleSystem particles;

    void Awake()
    {
        particles = GetComponent<ParticleSystem>();
        GameManager.Instance.BeganSquinting += new GameManager.SquintEvent(EmitParticles);
        GameManager.Instance.StoppedSquinting += new GameManager.SquintEvent(StopEmittingParticles);
    }

    void EmitParticles()
    {
        particles.emissionRate = emissionAmount;
    }

    void StopEmittingParticles()
    {
        particles.emissionRate = 0;
    }
}
