﻿using UnityEngine;
using System.Collections;

public class SquintVisible : MonoBehaviour {

   
    void Awake()
    {
        renderer.enabled = false;
        GameManager.Instance.BeganSquinting += new GameManager.SquintEvent(MakeVisible);
        GameManager.Instance.StoppedSquinting += new GameManager.SquintEvent(MakeInvisible);
    }

    void MakeVisible()
    {
        renderer.enabled = true;
    }

    void MakeInvisible()
    {
        renderer.enabled = false;
    }
}
