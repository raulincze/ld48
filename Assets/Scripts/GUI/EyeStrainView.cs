﻿using UnityEngine;
using System.Collections;

public class EyeStrainView : MonoBehaviour {

    public float dissapearIn = 2.0f;
    GUITexture toModify;
    private float lastTimeChanged;
    private bool visible;

    void Awake()
    {
        toModify = GetComponent<GUITexture>();
        SetAlpha(0f);
        visible = false;
        GameManager.Instance.ModifiedEyeStrain += new GameManager.EyeStrainChangedEvent(ModifyValue);
        lastTimeChanged = -dissapearIn+1;
    }

    void ModifyValue(float val)
    {
        if (!visible)
        {
            FadeIn();
            visible = true;
        }
        lastTimeChanged = Time.time;
        float actualValue = val / GameManager.Instance.eyeStrength * 500;
        toModify.pixelInset = new Rect(-actualValue/2, 0f, actualValue, 20f);
    }

    void Update()
    {
        if (Time.time - lastTimeChanged > dissapearIn && visible)
        {
            FadeOut();
            visible = false;
        }
    }

    void FadeIn()
    {
        LeanTween.cancel(gameObject);
        LeanTween.value(gameObject, "SetAlpha", toModify.color.a, 0.5f, 1f);
    }

    void FadeOut()
    {
        LeanTween.cancel(gameObject);
        LeanTween.value(gameObject, "SetAlpha", toModify.color.a, 0f, 1f);
    }

    void SetAlpha(float value)
    {
        Color newColor = toModify.color;
        newColor.a = value;
        toModify.color = newColor;
    }
}
